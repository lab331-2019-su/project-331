import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class ProAddComponent implements OnInit {

  constructor(private router:Router){}

  ngOnInit() {
  }

  add(): void {
    
    Swal.fire({
      title: 'Are you sure?',
      //text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, add it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Added!',
          '',
          'success'
        )
        this.router.navigate(["professor/list/detail/1"]);
      }
    })
  }
}
