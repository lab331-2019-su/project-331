import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {
  ngOnInit(): void {
    
  }

  constructor(public dialog: MatDialog,private router:Router){}
  
  submit(): void {
   
    Swal.fire({
      title: 'Are you sure?',
      //text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, submit it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'success',
          '',
          'success'
        )
        this.router.navigate(["/main"]);
      }
    })
  }

  back(){
    this.router.navigate(["/main"]);
  }

}
