import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentsViewComponent } from './view/students.view.component';
import { StudentsAddComponent } from './add/students.add.component';
import { StudentTableComponent } from './student-table/student-table.component';
import { EnrolledTableComponent } from './enrolled-table/enrolled-table.component';
import { StudentNavComponent } from './student-nav/student-nav.component';
import { MainPageComponent } from '../main-page/main-page.component';
const StudentRoutes: Routes = [ 

			// { path: 'view', component: StudentsViewComponent },
			// { path: 'add', component: StudentsAddComponent },
		 	// { path: 'list', component: StudentTableComponent},
			// { path: 'detail/:id', component: StudentsViewComponent},
			// { path: 'table', component: StudentTableComponent},
			// { path: 'enrolled', component: EnrolledTableComponent},

			{
				path: 'student', 
				component: StudentNavComponent,
				children : [
					{ path : '', redirectTo : 'list', pathMatch: 'full'},
					{ path : 'list', component : StudentTableComponent},
					{ path: 'add', component: StudentsAddComponent },
					{ path: 'detail/:id', component: StudentsViewComponent},
					{ path : 'enrolled', component : EnrolledTableComponent}
				]
		 },
			{ path: 'main', component: MainPageComponent}

		
		  ];
@NgModule({
  imports: [
    RouterModule.forRoot(StudentRoutes)
  ],
  exports: [
	RouterModule
	]
})
export class StudentRoutingModule { }
