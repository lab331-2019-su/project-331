import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentService } from './service/student-service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentsFileImplService } from './service/students-file-impl.service';
import { StudentsComponent } from './students/list/students.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsViewComponent } from './students/view/students.view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentRoutingModule } from './students/student-routing.module';
import { StudentTableComponent } from './students/student-table/student-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import {MatCardModule} from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';
import {AdminListComponent} from './admin/list/list.component';
import { AdminTableComponent } from './admin/admin-table/admin-table.component';
import {MainPageComponent} from './main-page/main-page.component';
import {AdminRoutingModule} from './admin/admin-routing.module';
import { ProfessorTableComponent } from './professor/professor-table/professor-table.component';
import {ProfessorRoutingModule} from './professor/professor-routing.module';
import { ActivityService } from './service/activity-service';
import { ActivityFileImplService } from './service/activity-file-impl.service';
import { Professorlist } from './professor/list/professor-list.component';
import { AddComponent } from './admin/add/add.component';
import { ViewComponent } from './professor/view/view.component';
import { MatMenuModule} from '@angular/material/menu';
import { ReactiveFormsModule } from '@angular/forms';
import { EnrolledTableComponent } from './students/enrolled-table/enrolled-table.component';
import { ConfirmationDialogComponent } from './shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialogModule} from '@angular/material';
import { MixdataFileImplService } from './service/mixdata-file-impl.service';
import { MixdataService } from './service/mixdata-service';
import { StudentNavComponent } from './students/student-nav/student-nav.component';
import { ProfessorNavComponent } from './professor/professor-nav/professor-nav.component';
import { AdminNavComponent } from './admin/admin-nav/admin-nav.component';
import {MatGridListModule,  MatSelectModule,MatExpansionModule} from '@angular/material';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { RegisterPageComponent } from './register-page/register-page.component';
import { ProAddComponent } from './professor/add/add.component';



@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    StudentTableComponent,
    AdminListComponent,
    AdminTableComponent,
    MainPageComponent,
    ProfessorTableComponent,
    Professorlist,
    AddComponent,
    ViewComponent,
    EnrolledTableComponent,
    ConfirmationDialogComponent,
    StudentNavComponent,
    ProfessorNavComponent,
    AdminNavComponent,
    RegisterPageComponent,
    ProAddComponent

    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    // MatSortModule,
    
    StudentRoutingModule,
    AdminRoutingModule,
    ProfessorRoutingModule,
    AppRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
   
    MatInputModule,
    MatCardModule,
    FlexLayoutModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMenuModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatGridListModule,
    MatSelectModule,
    MatExpansionModule,
    SweetAlert2Module
    
    
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
  providers: [
    { provide: StudentService, useClass: StudentsFileImplService },
    { provide: ActivityService, useClass: ActivityFileImplService },
    { provide: MixdataService, useClass: MixdataFileImplService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
ActivityService