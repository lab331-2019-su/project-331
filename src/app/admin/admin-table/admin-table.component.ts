import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { AdminTableDataSource } from './admin-table-datasource';
import Student from 'src/app/entity/student';
import { BehaviorSubject } from 'rxjs';
import { StudentService } from 'src/app/service/student-service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin-table',
  templateUrl: './admin-table.component.html',
  styleUrls: ['./admin-table.component.css']
})
export class AdminTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Student>;
  dataSource: AdminTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'studentId','name','surname','dob','email','password','image','ApproveBtn','RejectBtn'];
  students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private studentService: StudentService) { }
  ngOnInit() {
    this.studentService.getStudents()
      .subscribe(students => {
        this.dataSource = new AdminTableDataSource();
        this.dataSource.data = students;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        this.table.dataSource = this.dataSource;
        this.students = students;
        
      }
      )
    
  }

  ngAfterViewInit() {
    
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  approve(){
    
    Swal.fire({
      title: 'Are you sure?',
      //text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Approve it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Approved!',
          '',
          'success'
        )
      }
    })
  
}

reject(){
  
    Swal.fire({
      title: 'Are you sure?',
      //text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Reject it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Rejected!',
          '',
          'success'
        )
      }
    })
  
}
}
