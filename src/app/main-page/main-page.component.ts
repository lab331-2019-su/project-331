import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  constructor(private router: Router) { }

  email: string;
  password: string;

  ngOnInit() {
    
  }
  login() : void {
    
    

    if(this.email == 'admin' && this.password == 'admin'){
      
     this.router.navigate(["admin"]);
     const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    })
     Toast.fire({
      type: 'success',
      title: 'Signed in successfully'
    })
    } 
    else if (this.email == 'professor' && this.password == 'professor') {
      this.router.navigate(["professor"]);
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        type: 'success',
        title: 'Signed in successfully'
      })
    } 
    else if (this.email == 'student' && this.password == 'student') {
      this.router.navigate(["student"]);
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        type: 'success',
        title: 'Signed in successfully'
      })
    } else {
      //alert("Invalid Username or Password");
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        type: 'error',
        title: 'Signed in failed'
      })
    }

  }

  register(): void{
    this.router.navigate(["/register"]);
  }
}
