import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StudentService } from './student-service';
import { Observable } from '../../../node_modules/rxjs';
import { map } from 'rxjs/operators';
import Student from '../entity/student';
import Activity from '../entity/activity';
import { ActivityService } from './activity-service';
@Injectable({
  providedIn: 'root'
})

export class StudentsFileImplService extends StudentService {

  constructor(private http: HttpClient) {
    super();
  }
  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>('assets/people.json');
  }
  getStudent(id :number): Observable<Student> {
	  return this.http.get<Student[]>('assets/people.json')
	  .pipe(map(student => {
		  const output: Student = 
			( student as Student[]).find
				(student => student.id === +id);
				return output;
    }));
    
    
}

}