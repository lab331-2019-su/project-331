import { Observable } from '../../../node_modules/rxjs';
import Student from '../entity/student';
import Mixdata from '../entity/mixdata';

export abstract class MixdataService{
     abstract getMixdatas(): Observable<Mixdata[]>;
     abstract getMixdata(id : number): Observable<Mixdata>;
     
}
