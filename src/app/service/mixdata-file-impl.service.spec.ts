import { TestBed } from '@angular/core/testing';

import { MixdataFileImplService } from './mixdata-file-impl.service';

describe('MixdataFileImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MixdataFileImplService = TestBed.get(MixdataFileImplService);
    expect(service).toBeTruthy();
  });
});
